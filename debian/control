Source: python-treq
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Orestis Ioannou <orestis@oioannou.com>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-sphinx,
 python3-all,
 python3-attr,
 python3-hyperlink (>= 21.0.0),
 python3-incremental,
 python3-openssl (>= 0.15.1),
 python3-requests (>= 2.1.0),
 python3-service-identity,
 python3-setuptools,
 python3-twisted (>= 18.7.0),
 python3-typing-extensions,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://treq.readthedocs.org/en/latest/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-treq.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-treq

Package: python3-treq
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Higher level API for making HTTP requests with Twisted (Python 3)
 Treq is a HTTP library inspired by requests written on top of Twisted.
 It provides a simple, higher level API for making HTTP requests when using
 Twisted.
 .
 This is the Python 3 package.

Package: python-treq-doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Built-Using:
 ${sphinxdoc:Built-Using},
Section: doc
Description: Higher level API for making HTTP requests with Twisted (doc)
 Treq is a HTTP library inspired by requests written on top of Twisted.
 It provides a simple, higher level API for making HTTP requests when using
 Twisted.
 .
 This package contains the HTML documentation.
